package com.weixun.cms.controller;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.weixun.cms.service.SiteService;
import com.weixun.utils.ajax.AjaxMsg;
import com.weixun.utils.file.FileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FileController extends Controller{
    SiteService siteService = new SiteService();

    /**
     * 根据站点信息获取配置路径
     * 目前只实现了单站点
     * @return 站点信息
     */
    private Record  getpath()
    {
        List<Record>  records =siteService.findList("");
        return records.get(0);
    }


    /**
     * 文章标题图片上传
     * 区别于ueditor中的文章内容图片上传
     *
     */
    public void upload()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        try {
            //设置文件上传子目录
            String path="uploads/images/";
            //获取上传的文件
            UploadFile upload = getFile("file",path);
            File file = upload.getFile();
            //获取文件名
            String extName = FileUtils.getFileExt(file.getName());
            //获取文件上传的父目录
            String filePath = upload.getUploadPath();
            //时间命名文件
            String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + extName;
            //重命名原来的文件
            file.renameTo(new File(filePath+fileName));
            //重新组合文件路径  ip+文件目录+文件名
            String fileUrl=getpath().getStr("site_domain")+File.separator+path+fileName;

            ajaxMsg.setState("success");
            ajaxMsg.setMsg(fileUrl);

        } catch (Exception e) {
            e.printStackTrace();
            ajaxMsg.setState("fail");
        }

        renderJson(ajaxMsg);
    }





//    public void upload()
//    {
//        AjaxMsg ajaxMsg = new AjaxMsg();
//        try {
//            UploadFile uploadFile = getFile();
//            File file=uploadFile.getFile();
//            System.out.println("--------file--------");
//            String parent_path = getpath().getStr("site_static")+File.separator;
//            String path = "uploads/images/"+file.getName();
//            File upFile = new File(parent_path+path);
//            if (FileUtils.SaveFile(parent_path+path)) {
//                System.out.println("==========" + upFile.getPath());
//                ajaxMsg.setState("success");
//                ajaxMsg.setMsg(getpath().getStr("site_domain")+File.separator+path);
//
//                File delFile = new File(uploadFile.getFileName());
//                if(delFile.exists()){
//                    delFile.delete();
//                }
//
//            }else
//            {
//                ajaxMsg.setState("fail");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            ajaxMsg.setState("fail");
//        }
//
//        renderJson(ajaxMsg);
//    }
}
