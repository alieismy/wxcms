<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/12/13
  Time: 下午9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">--%>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<%--查询条件以及操作按钮--%>
<br>
<div class="layui-form">
    <%--查询条件--%>
    <div class="layui-form-item layui-form-pane">
        <label class="layui-form-label">角色名称</label>
        <div class="layui-input-inline">
            <input type="text" name="role_name" required  lay-verify="required" placeholder="请输入角色名称" autocomplete="off" class="layui-input" id="role_name">
        </div>
        <%--操作按钮--%>
        <button class="layui-btn"  id="search">
            <i class="layui-icon">&#xe615;</i>搜索
        </button>
        <shiro:hasPermission name="role:add">
        <button class="layui-btn" id="add">
            <i class="layui-icon">&#xe608;</i>添加
        </button>
        </shiro:hasPermission>
    </div>


</div>


<%--分页数据列表--%>
<div class="layui-form">
    <table id="tablelist" class="layui-table" lay-data="{height: 'full-80', cellMinWidth: 80, page: true, limit:30, url:'${basePath}/role/pages'}" lay-filter="table">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox'}">ID</th>
            <th lay-data="{field:'role_pk', width:100, sort: true}">ID</th>
            <th lay-data="{field:'role_name', width:100}">角色名称</th>
            <th lay-data="{field:'role_time', sort: true, minWidth: 100, align: 'center'}">录入日期</th>
            <th lay-data="{field:'role_status', sort: true, minWidth: 100, align: 'center',templet:'#switchTpl'}">状态</th>
            <th lay-data="{field:'role_remark', sort: true, minWidth: 100, align: 'center'}">备注</th>
            <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barDemo'}"></th>
        </tr>
        </thead>
    </table>
</div>

<script type="text/html" id="barDemo">
<shiro:hasPermission name="role:edit">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
</shiro:hasPermission>
<shiro:hasPermission name="role:delete">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</shiro:hasPermission>
<shiro:hasPermission name="role:grant">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="grant">授权</a>
</shiro:hasPermission>
</script>


<script src="${basePath}/static/plugins/layui/layui.js"></script>
<script type="text/html" id="switchTpl">
    <!-- 这里的 checked 的状态只是演示 -->
    {{#
    if(d.role_status === '0'){
    }}
    <span style="color: #1110f5;">启用</span>
    {{#
    }
    }}
    {{#
    if(d.role_status === '1'){
    }}
    <span style="color: #f5120f;">禁用</span>
    {{#
    }
    }}
</script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function(){
        var $ = layui.jquery;
        var table = layui.table;

        //监听表格复选框选择
        table.on('checkbox(table)', function(obj){
            console.log(obj)
        });

        //监听工具条
        table.on('tool(table)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                role_delete(data.role_pk);
            } else if(obj.event === 'edit'){
                edit(data.role_pk);
            }else if(obj.event === 'grant'){
                grant_authority(data.role_pk);
            }

        });


        $('#add').on('click', function() {
            layer.open({
                title: '添加',
                maxmin: true,
                type: 2,
                content: 'edit.jsp',
                area: ['500px', '450px'],
                success: function(layero, index) {

                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        });


        //过滤查询
        $("#search").on('click',function () {
            //执行重载
            table.reload('tablelist', {
                url: '${basePath}/role/pages',

                where: {
//                    key: {
                        role_name:$("#role_name").val()
//                    }
                  },
                  page: {
                      curr: 1 //重新从第 1 页开始
                  }

            });
          
            
        });


        //编辑操作
        edit = function (data){
            layer.open({
                title : '编辑',
                maxmin : true,
                type : 2,
                content : 'edit.jsp',
                area : ['500px','450px'],
                success : function(layero,index){
                    var body = layer.getChildFrame('body', index); //巧妙的地方在这里哦
                    body.contents().find("#role_pk").val(data);
                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        };

        //授权
        grant_authority = function (data){
            layer.open({
                title : '授权',
                maxmin : true,
                type : 2,
                content : 'authority.jsp',
                area : ['300px','500px'],
                success : function(layero,index){
                    var body = layer.getChildFrame('body', index); //巧妙的地方在这里哦
                    body.contents().find("#role_pk").val(data);
                    layer.iframeAuto(index);
                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        }


        //删除记录
        role_delete = function (data) {
            var flag = false;
            layer.confirm("确认删除此数据吗？", {icon: 3, title: '提示'},
                function (index) {      //确认后执行的操作
                    $.ajax({
                        type: 'post',
                        url: '${basePath}/role/delete',
                        data: {role_pk: data},
                        success: function (response) {
                            if (response.state == "success") {
                                layer.close(index);
                                parent.layer.msg("删除成功");
                                table.reload("tablelist");
                            } else if (response.state == "fail") {
                                parent.layer.alert(response.msg);
                            }
                        },
                        error: function (response) {
                            layer.close(index);
                            parent.layer.alert(response.msg);
                        }
                    });
                },
                function (index) {      //取消后执行的操作
                    flag = false;
                });
        }

    });
</script>


</body>
</html>
